﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Weathery
{
    public static class AccuWeather
    {      
        public static string AccuweatherLocationSearch(string url)
        {
            var response = new System.Net.Http.HttpClient();
            return Task.Run(() => response.GetStringAsync(new Uri(string.Format(url, string.Empty)))).Result;
        }
    }
}
