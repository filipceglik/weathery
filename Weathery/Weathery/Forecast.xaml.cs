﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Weathery
{
    /// <summary>
    /// Interaction logic for Forecast.xaml
    /// </summary>
    public partial class Forecast : Page
    {
        private Task<Stream> client;
        private string selectedItem;

        public Forecast(string selectedItem)
        {
            this.selectedItem = selectedItem;
            string selectedCity;
            selectedCity = (string)selectedItem;
            InitializeComponent();
            var result = AccuWeather.AccuweatherLocationSearch($"http://api.wunderground.com/api/58bbcc38f2f0f7fd/conditions/q/zmw:" + selectedItem + ".json");
            //var wat = JsonConvert.SerializeObject(lista);
            var costam = JsonConvert.DeserializeObject<ConditionsAPI.RootObject>(result);

            var wunresult = Wunderground.WundergroundForecast($"http://api.wunderground.com/api/58bbcc38f2f0f7fd/forecast/q/zmw:" + selectedCity + ".json");
            var forecast = JsonConvert.DeserializeObject<WunResult.RootObject>(wunresult);
            //string URL;

            //var photodown = new HttpClient();

            //List<WunResult.Forecastday2> listawynikow = new List<WunResult.Forecastday2>();

           

            //var linqresult = from forecast in forecast.forecast.txt_forecast.forecastday
            //                 where pe

            //bitmap.SetSource(memoryStream.AsRandonAccessStream());
            //imageControl.Source = bitmap;

            // image = costam.current_observation.image;
            //city.Content = "Temperature: " + costam.current_observation.temperature_string;

            temp.Content = costam.current_observation.temp_c.ToString();
            city.Content = costam.current_observation.display_location.full.ToString() + ", " + costam.current_observation.display_location.state.ToString();
            condition.Content = costam.current_observation.weather.ToString();
            realfeel.Content = "Feels Like: " + costam.current_observation.feelslike_c.ToString();
           // prediction.Content = forecast.
        }


    }
}
