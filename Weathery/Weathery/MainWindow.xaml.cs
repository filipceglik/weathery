﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Newtonsoft.Json;

namespace Weathery
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private string citystring;

        public MainWindow()
        {
            InitializeComponent();
        }

        

        //public Page1 content { get; private set; }

       

        private void locationbox_TextChanged(object sender, TextChangedEventArgs e)
        {

        }





        private void searchbutton_Click(object sender, RoutedEventArgs e)
        {
            citystring = locationbox.Text;
            
            this.Content = new json($"http://autocomplete.wunderground.com/aq?query=" + citystring).ShowDialog();
        }
    }
}
