﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Weathery
{
    /// <summary>
    /// Interaction logic for json.xaml
    /// </summary>
    public partial class json : Window
    {
        public json(string jsonek)
        {
            InitializeComponent();
            var backshit = AccuWeather.AccuweatherLocationSearch(jsonek);
            //var wat = JsonConvert.SerializeObject(lista);
            var costam = JsonConvert.DeserializeObject<RootObject>(backshit);
            foreach (var item in costam.RESULTS)
            {
                //searchresults.Items.Add(item);
                searchresults.Items.Add(item.name);
                searchresults.Items.Add(item.zmw);
            }
        }

        public void searchresults_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            searchresults.MouseDoubleClick += Searchresults_MouseDoubleClick;

        }

        private void Searchresults_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            //this.Content = new Forecast(searchresults);
        }

        private void checkforecast_Click(object sender, RoutedEventArgs e)
        {
            
        }

        private void checkforecast_Click_1(object sender, RoutedEventArgs e)
        {
            if (searchresults.SelectedItem != null)
            {
                searchresults.SelectedIndex += 1;
                this.Content = new Forecast(searchresults.SelectedItem.ToString());
            }
        }
    }
}
